package com.highlightmusicgroup.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.highlightmusicgroup.R;
import com.highlightmusicgroup.activities.HomeScreen;
import com.highlightmusicgroup.activities.Login;
import com.highlightmusicgroup.application.HMG;
import com.highlightmusicgroup.data.models.asset.SongDetails;
import com.highlightmusicgroup.data.models.music.MusicResponse;
import com.highlightmusicgroup.data.remote.APIUtils;
import com.highlightmusicgroup.databinding.FragmentNowPlayingBinding;
import com.highlightmusicgroup.playerManager.MediaController;
import com.highlightmusicgroup.util.AppManageInterface;
import com.highlightmusicgroup.util.AppUtil;
import com.highlightmusicgroup.util.Const;
import com.highlightmusicgroup.util.Prefs;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.highlightmusicgroup.adapters.HomeCategoryAdapter.isfromDialogMusic;

public class NowPlayingFragment extends Fragment {

    private static NowPlayingFragment instance = null;
    FragmentNowPlayingBinding binding;

    private AppManageInterface appManageInterface;
    private String path;
    private String id;
    private String shareDetails;
    private Context context;
    private FragmentManager fragmentManager;
    private Tracker mTracker;
    private ArrayList<SongDetails> songs = new ArrayList<>();

    public static synchronized NowPlayingFragment getInstance() {
        return instance;
    }

    public static synchronized NowPlayingFragment newInstance() {
        return instance = new NowPlayingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTracker = HMG.getDefaultTracker();
        binding = FragmentNowPlayingBinding.inflate(inflater, container, false);
        HomeScreen.isPipModeEnabled = true;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Now Playing");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        binding.loader.setVisibility(View.GONE);
        binding.songLoaderNowPopup.setVisibility(View.GONE);

        appManageInterface.miniPlayerVisibilityGone(true);
        appManageInterface.bottomanduppermanage(true);
//        appManageInterface.closeVideoBottom();

        if (getArguments() != null) {
            path = getArguments().getString("path");
            id = getArguments().getString("id");
            getMusicDetail(path, id);
        }
        Log.d("mytag","here is now playing screen radio status is--------------------"+HomeScreen.radioStatus);
//        if(HomeScreen.radioStatus == 1){
//            Glide.with(this)
//                    .load(R.raw.radio_gif)
//                    .into(binding.radio);
//        }else{
//            Glide.with(this)
//                    .load(R.drawable.selector_home_radio)
//                    .into(binding.radio);
//        }
        binding.home.setSelected(true);
        binding.songPlayPauseNowPlaying.setSelected(!MediaController.getInstance().isAudioPaused());

//        binding.back.setOnClickListener(v -> appManageInterface.go_back());

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isfromDialogMusic) {
                    appManageInterface.showMainAcreen();
                } else {
                    appManageInterface.go_back();
                }
            }
        });

        binding.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, HomeScreen.class);
                startActivity(i);
            }
        });

        binding.liveTvPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, LiveTvFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManageInterface.setVisibleStatus(false, false, false, false, false);
                AppUtil.setup_Fragment(fragmentManager, RadioFragment.newInstance(), "", getResources().getString(R.string.radio), getResources().getString(R.string.live_radio), "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, VideoFragment.newInstance(), "", "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "FavouritesSongsList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "PlaylistList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.drawerPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, MenuFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.favourite.setOnClickListener(view1 -> appManageInterface.addRemoveAudioToFavourites(songs.get(0), ""));

        binding.playlist.setOnClickListener(view12 -> {
            appManageInterface.showPlaylistOptions(songs.get(0));
        });

        binding.share.setOnClickListener(view13 -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareDetails);
            sendIntent.setType("text/plain");
            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);
            appManageInterface.callTotalShared(songs);
        });

        binding.songPlayPauseNowPlaying.setOnClickListener(view1 -> {
            if (AppUtil.isInternetAvailable(context)) {
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    if (MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                    } else {
                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                    }
                } else {
                }
            } else {
                AppUtil.show_Snackbar(context, binding.popupPlayerContainer, getResources().getString(R.string.no_internet_connection), false);
            }
        });
        binding.songNextPopup.setOnClickListener(view2 -> {
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                MediaController.getInstance().playNextSong();
            }
        });
        binding.songPreviousPopup.setOnClickListener(view3 -> {
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                MediaController.getInstance().playPreviousSong();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onDestroyView() {
        appManageInterface.miniPlayerVisibilityGone(false);
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void getMusicDetail(String path, String id) {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.GONE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
                jsonObject.put("song_id", id);
                jsonObject.put("menu_id", path);
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_music_detail = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_now_playing(get_music_detail, "Tracks").enqueue(new Callback<MusicResponse>() {
                @Override
                public void onResponse(@NonNull Call<MusicResponse> call, @NonNull Response<MusicResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                songs = response.body().getData();
                                binding.songPlayPauseNowPlaying.setSelected(!MediaController.getInstance().isAudioPaused());
                                Glide
                                        .with(HMG.applicationContext)
                                        .load(response.body().getData().get(0).getSongImage())
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .centerCrop()
                                        .placeholder(R.drawable.app_logo)
                                        .error(R.drawable.app_logo)
                                        .into(binding.assetImage);

                                Glide
                                        .with(HMG.applicationContext)
                                        .load(response.body().getData().get(0).getSongImage())
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .centerCrop()
                                        .placeholder(R.drawable.app_logo)
                                        .error(R.drawable.app_logo)
                                        .into(binding.ivBlurPopup);

                                binding.assetName.setText(response.body().getData().get(0).getSongName());
                                binding.assetArtist.setText(response.body().getData().get(0).getSongArtist());
                                binding.favCount.setText(response.body().getData().get(0).getFavouritesCount());
                                binding.playlistCount.setText(response.body().getData().get(0).getPlaylistStatusCount());
                                binding.shareCount.setText(response.body().getData().get(0).getTotalShared());
                                binding.playsCount.setText(response.body().getData().get(0).getTotalPlayed());
                                binding.favourite.setSelected(response.body().getData().get(0).getFavouritesStatus());
                                binding.playlist.setSelected(response.body().getData().get(0).getPlaylistStatus());
                                binding.share.setSelected(response.body().getData().get(0).getTotalSharedStatus());
                                binding.loader.setVisibility(View.GONE);

                                shareDetails = "Song Name: " + response.body().getData().get(0).getSongName() + "\n\n" + "Song URL: " + response.body().getData().get(0).getSong()
                                        + "\n\n" + "Artist Name: " + response.body().getData().get(0).getSongArtist() + "\n\n" + "App Name: " + context.getResources().getString(R.string.app_name);

                            } else {
                                binding.loader.setVisibility(View.GONE);
                                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                                final AlertDialog dialog = new AlertDialog.Builder(context)
                                        .setCancelable(false)
                                        .setView(dialog_view)
                                        .show();

                                if (dialog.getWindow() != null)
                                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                    dialog.dismiss();
                                });
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MusicResponse> call, @NonNull Throwable t) {
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    public void setShareCount(boolean totalSharedStatus, String totalShared) {
        binding.share.setSelected(totalSharedStatus);
        binding.shareCount.setText(totalShared);
    }

    public void setFav(boolean favoritesStatus, Integer favoritesCount) {
        binding.favourite.setSelected(favoritesStatus);
        binding.favCount.setText(String.valueOf(favoritesCount));
    }

    public void setSongLoader(boolean isLoaderVisible) {
        binding.songLoaderNowPopup.setVisibility(isLoaderVisible ? View.VISIBLE : View.GONE);
        binding.songPlayPauseNowPlaying.setVisibility(isLoaderVisible ? View.GONE : View.VISIBLE);
    }
}
