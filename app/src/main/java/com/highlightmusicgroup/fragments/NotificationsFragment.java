package com.highlightmusicgroup.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.highlightmusicgroup.R;
import com.highlightmusicgroup.Services.FloatingWidgetService;
import com.highlightmusicgroup.activities.HomeScreen;
import com.highlightmusicgroup.activities.Login;
import com.highlightmusicgroup.adapters.NotificationsAdapter;
import com.highlightmusicgroup.application.HMG;
import com.highlightmusicgroup.data.models.notifications.NotificationsResponse;
import com.highlightmusicgroup.data.remote.APIUtils;
import com.highlightmusicgroup.databinding.FragmentNotificationsBinding;
import com.highlightmusicgroup.util.AppManageInterface;
import com.highlightmusicgroup.util.AppUtil;
import com.highlightmusicgroup.util.Const;
import com.highlightmusicgroup.util.Prefs;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsFragment extends Fragment {

    FragmentNotificationsBinding binding;
    private static NotificationsFragment instance = null;

    private AppManageInterface appManageInterface;
    private String path;
    private String type;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private Activity activity;
    private Tracker mTracker;

    public static synchronized NotificationsFragment getInstance() {
        return instance;
    }

    public static synchronized NotificationsFragment newInstance() {
        return instance = new NotificationsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTracker = HMG.getDefaultTracker();
        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        FloatingWidgetService.hidePipPlayer();
        HomeScreen.isPipModeEnabled = true;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Notifications");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        binding.loader.setVisibility(View.GONE);
        binding.noDataFound.setVisibility(View.GONE);
        binding.listing.setVisibility(View.GONE);
        activity = getActivity();
//        appManageInterface.miniPlayerVisibilityGone(true);
//        appManageInterface.bottomanduppermanage(true);
        appManageInterface.closeVideoBottom();
        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
        }
        if(HomeScreen.radioStatus == 1){
            Glide.with(this)
                    .load(R.raw.radio_gif)
                    .into(binding.radio);
        }else{
            Glide.with(this)
                    .load(R.drawable.selector_home_radio)
                    .into(binding.radio);
        }
        binding.tvTitle.setText("Notifications");
        binding.drawerPopup.setSelected(true);
        init();
        call_notifications();
    }

    private void init() {
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManageInterface.go_back();
            }
        });

        binding.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, HomeScreen.class);
                startActivity(i);
            }
        });

        binding.liveTvPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, LiveTvFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManageInterface.setVisibleStatus(false, false, false, false, false);
                AppUtil.setup_Fragment(fragmentManager, RadioFragment.newInstance(), "", getResources().getString(R.string.radio), getResources().getString(R.string.live_radio), "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, VideoFragment.newInstance(), "", "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "FavouritesSongsList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "PlaylistList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.drawerPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, MenuFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void call_notifications() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_notifications = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_notifications(call_notifications).enqueue(new Callback<NotificationsResponse>() {
                @Override
                public void onResponse(@NonNull Call<NotificationsResponse> call, @NonNull Response<NotificationsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {

                                binding.listing.setHasFixedSize(true);
                                binding.listing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                                NotificationsAdapter notificationsAdapter = new NotificationsAdapter(context, response.body().getData());
                                binding.listing.setAdapter(notificationsAdapter);
                                binding.listing.setVisibility(View.VISIBLE);
                                binding.noDataFound.setVisibility(View.GONE);

                            } else {
                                binding.noDataFound.setVisibility(View.VISIBLE);
                            }
                                binding.loader.setVisibility(View.GONE);
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.noDataFound.setVisibility(View.VISIBLE);
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });

                        }
                    } else {
                        binding.noDataFound.setVisibility(View.VISIBLE);
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<NotificationsResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.noDataFound.setVisibility(View.VISIBLE);
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            binding.noDataFound.setVisibility(View.VISIBLE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }
}
