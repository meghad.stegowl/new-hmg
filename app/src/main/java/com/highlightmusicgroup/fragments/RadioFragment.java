package com.highlightmusicgroup.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.highlightmusicgroup.R;
import com.highlightmusicgroup.Services.FloatingWidgetService;
import com.highlightmusicgroup.activities.HomeScreen;
import com.highlightmusicgroup.activities.Login;
import com.highlightmusicgroup.application.HMG;
import com.highlightmusicgroup.data.models.asset.SongDetails;
import com.highlightmusicgroup.data.models.music.MusicResponse;
import com.highlightmusicgroup.data.remote.APIUtils;
import com.highlightmusicgroup.databinding.FragmentRadioBinding;
import com.highlightmusicgroup.playerManager.MediaController;
import com.highlightmusicgroup.util.AppManageInterface;
import com.highlightmusicgroup.util.AppUtil;
import com.highlightmusicgroup.util.Const;
import com.highlightmusicgroup.util.Prefs;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class RadioFragment extends Fragment {

    private static RadioFragment instance = null;
    FragmentRadioBinding binding;

    private AppManageInterface appManageInterface;
    private String path, type, pageTitle;
    private Context context;
    private FragmentManager fragmentManager;
    private Tracker mTracker;
    private ArrayList<SongDetails> data = new ArrayList<>();
    public static int radioPauseStatus = 0;


    public static synchronized RadioFragment getInstance() {
        return instance;
    }

    public static synchronized RadioFragment newInstance() {
        return instance = new RadioFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTracker = HMG.getDefaultTracker();
        binding = FragmentRadioBinding.inflate(inflater, container, false);
        FloatingWidgetService.hidePipPlayer();
        HomeScreen.isPipModeEnabled = true;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Radio");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        appManageInterface.closeVideoBottom();

        binding.loader.setVisibility(View.GONE);
        binding.radio.setSelected(true);
        binding.songLoaderRadio.setVisibility(View.GONE);
        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
//            binding.title.setText(pageTitle);
            getRadio();
            automaticallyPlay();
        } else {
            binding.loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(d -> {
                dialog.dismiss();
                fragmentManager.popBackStack();
            });
        }

        binding.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, HomeScreen.class);
                startActivity(i);
            }
        });
        binding.liveTvPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, LiveTvFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, RadioFragment.newInstance(), "", getResources().getString(R.string.radio), getResources().getString(R.string.live_radio), "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, VideoFragment.newInstance(), "", "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "FavouritesSongsList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "PlaylistList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.drawerPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, MenuFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        if (MediaController.getInstance().getPlayingSongDetail() != null &&
                MediaController.getInstance().getPlayingSongDetail().getType() != null
                && MediaController.getInstance().getPlayingSongDetail().getType().equals(context.getResources().getString(R.string.radio))) {
            binding.songPlayPauseRadio.setSelected(!MediaController.getInstance().isAudioPaused());
            Log.d("mytag", "here is played psause in methoddddddddddddddddddddddd-----------------------");
            Glide.with(this)
                    .load(R.raw.radio_gif)
                    .into(binding.radio);
            HomeScreen.radioStatus = 1;
        } else {
            Log.d("mytag", "here is played psause in methodffffffffffffffffffffffff-----------------------");
            HomeScreen.radioStatus = 0;
            Glide.with(this)
                    .load(R.drawable.selector_home_radio)
                    .into(binding.radio);
            binding.songPlayPauseRadio.setSelected(false);
        }
        setRadioSongLoader(false);

        binding.songPlayPauseRadio.setOnClickListener(view13 -> {
            if (AppUtil.isInternetAvailable(context)) {
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    if (MediaController.getInstance().getPlayingSongDetail().getType() != null &&
                            MediaController.getInstance().getPlayingSongDetail().getType().toLowerCase().equals(context.getResources().getString(R.string.radio).toLowerCase())) {
                        try {
                            if (MediaController.getInstance().getPlayingSongDetail().getSongId() != null &&
                                    MediaController.getInstance().getPlayingSongDetail().getSongId().equals(data.get(0).getSongId())) {
                                if (MediaController.getInstance().isAudioPaused()) {
                                    MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                                    Log.d("mytag", "here is played psause in methodpppppppppppppppp----------------------");
                                    Glide.with(this)
                                            .load(R.raw.radio_gif)
                                            .into(binding.radio);
                                    HomeScreen.radioStatus = 1;
                                } else {
                                    Log.d("mytag", "here is played psause in method,,,,,,,,,,,,,,,,,,,,,,,-----------------------");
                                    HomeScreen.radioStatus = 0;
                                    radioPauseStatus = 1;
                                    Glide.with(this)
                                            .load(R.drawable.selector_home_radio)
                                            .into(binding.radio);
                                    MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                                }
                            } else {
                                Log.d("mytag", "here is played psause in methodkkkkkkkkkkkkkkkkkkkkkk-----------------------");
                                HomeScreen.radioStatus = 1;
                                Glide.with(this)
                                        .load(R.raw.radio_gif)
                                        .into(binding.radio);
                                MediaController.getInstance().setPlaylist(data, data.get(0));
                            }
                        }catch (Exception e){}
                    } else {
                        Log.d("mytag", "here is played psause in method9999999999999999999999-----------------------");
                        MediaController.getInstance().setPlaylist(data, data.get(0));
                        Glide.with(this)
                                .load(R.raw.radio_gif)
                                .into(binding.radio);
                        HomeScreen.radioStatus = 1;
                    }
                } else {
                    try {
                        Log.d("mytag", "here is played psause in method88888888888888-----------------------");
                        HomeScreen.radioStatus = 1;
                        MediaController.getInstance().setPlaylist(data, data.get(0));
                        Glide.with(this)
                                .load(R.raw.radio_gif)
                                .into(binding.radio);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                AppUtil.show_Snackbar(context, binding.container, getResources().getString(R.string.no_internet_connection), false);
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onDestroyView() {
        appManageInterface.setVisibleStatus(false, false, true, false, false);
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void automaticallyPlay() {
        if (MediaController.getInstance().getPlayingSongDetail() != null) {
            if (MediaController.getInstance().getPlayingSongDetail().getType() != null &&
                    MediaController.getInstance().getPlayingSongDetail().getType().toLowerCase().equals(context.getResources().getString(R.string.radio).toLowerCase())) {
                try {
                    if (MediaController.getInstance().getPlayingSongDetail().getSongId() != null &&
                            MediaController.getInstance().getPlayingSongDetail().getSongId().equals(data.get(0).getSongId())) {
                        if (MediaController.getInstance().isAudioPaused()) {
                            Glide.with(this)
                                    .load(R.raw.radio_gif)
                                    .into(binding.radio);
                            MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                            HomeScreen.radioStatus = 1;
                            Log.d("mytag", "here is played psause in method33333333333333333-----------------------");
                        }else {
                           /* Log.d("mytag", "here is played psause in method4444444444444444-----------------------");
                            Glide.with(this)
                                    .load(R.drawable.selector_home_radio)
                                    .into(binding.radio);
                            Const.isRadioPlayed = false;*/
                        } /*else {
                            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                        }*/
                    } else {
                        Log.d("mytag", "here is played psause in method555555555555555-----------------------");
                        try {
                            HomeScreen.radioStatus = 1;
                            Glide.with(this)
                                    .load(R.raw.radio_gif)
                                    .into(binding.radio);
                            MediaController.getInstance().setPlaylist(data, data.get(0));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    Log.d("mytag", "here is played psause in method66666666666666666666666-----------------------");
                    HomeScreen.radioStatus = 1;
                    Glide.with(this)
                            .load(R.raw.radio_gif)
                            .into(binding.radio);
                    MediaController.getInstance().setPlaylist(data, data.get(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                Log.d("mytag", "here is played psause in method77777777777777-----------------------");
                HomeScreen.radioStatus = 1;
                Glide.with(this)
                        .load(R.raw.radio_gif)
                        .into(binding.radio);
                MediaController.getInstance().setPlaylist(data, data.get(0));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getRadio() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_radio = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_radio(call_radio, "32", "Radio").enqueue(new Callback<MusicResponse>() {
                @Override
                public void onResponse(@NonNull Call<MusicResponse> call, @NonNull Response<MusicResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                binding.assetTitle.setText(response.body().getData().get(0).getSongName());
                                response.body().getData().get(0).setType(context.getResources().getString(R.string.radio));
                                response.body().getData().get(0).setSongId(response.body().getData().get(0).getSongId());
                                response.body().getData().get(0).setSong(response.body().getData().get(0).getSong());
                                response.body().getData().get(0).setSongName(response.body().getData().get(0).getSongName());
                                response.body().getData().get(0).setSongImage(response.body().getData().get(0).getSongImage());
                                data = response.body().getData();
                                automaticallyPlay();
                                Glide
                                        .with(HMG.applicationContext)
                                        .load(response.body().getData().get(0).getSongImage())
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .centerCrop()
                                        .placeholder(R.drawable.app_logo)
                                        .into(binding.assetImage);
                                binding.loader.setVisibility(View.GONE);
                            } else {
                                binding.loader.setVisibility(View.GONE);
                            }
                        } else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MusicResponse> call, @NonNull Throwable t) {
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    public void setRadioSongLoader(boolean isLoaderVisible) {
        binding.songLoaderRadio.setVisibility(isLoaderVisible ? View.VISIBLE : View.GONE);
        binding.songPlayPauseRadio.setVisibility(isLoaderVisible ? View.GONE : View.VISIBLE);
        int progressbarValue = 0;
        if (MediaController.getInstance().getPlayingSongDetail() != null && MediaController.getInstance().getPlayingSongDetail().getType() != null && MediaController.getInstance().getPlayingSongDetail().getType().equals(context.getResources().getString(R.string.radio)))
            progressbarValue = (int) (MediaController.getInstance().getPlayingSongDetail().audioProgress * 100);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.songSeekBarRadio.setProgress(progressbarValue, true);
        } else {
            binding.songSeekBarRadio.setProgress(progressbarValue);
        }
    }

    public void setRadioPLayPause() {

        if (MediaController.getInstance().getPlayingSongDetail() != null &&
                MediaController.getInstance().getPlayingSongDetail().getType() != null
                && MediaController.getInstance().getPlayingSongDetail().getType().equals(context.getResources().getString(R.string.radio))) {
            binding.songPlayPauseRadio.setSelected(!MediaController.getInstance().isAudioPaused());
            Log.d("mytag", "here is played psause in method11111111111111111-----------------------");
        } else {
            binding.songPlayPauseRadio.setSelected(false);
            HomeScreen.radioStatus = 0;
            Glide.with(this)
                    .load(R.drawable.selector_home_radio)
                    .into(binding.radio);
            Log.d("mytag", "here is played psause in method222222222222-----------------------");
        }
    }

    public void setSecondaryProgress(int progressbarValue) {
        if (MediaController.getInstance().getPlayingSongDetail() != null && MediaController.getInstance().getPlayingSongDetail().getType() != null && MediaController.getInstance().getPlayingSongDetail().getType().equals(context.getResources().getString(R.string.radio)))
            binding.songSeekBarRadio.setSecondaryProgress(progressbarValue);
    }
}
