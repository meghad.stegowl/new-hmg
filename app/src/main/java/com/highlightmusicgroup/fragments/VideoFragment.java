package com.highlightmusicgroup.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.highlightmusicgroup.R;
import com.highlightmusicgroup.activities.HomeScreen;
import com.highlightmusicgroup.activities.Login;
import com.highlightmusicgroup.adapters.VideoListingAdapter;
import com.highlightmusicgroup.application.HMG;
import com.highlightmusicgroup.data.models.video.MenuItemsResponse;
import com.highlightmusicgroup.data.remote.APIUtils;
import com.highlightmusicgroup.databinding.FragmentVideoBinding;
import com.highlightmusicgroup.util.AppManageInterface;
import com.highlightmusicgroup.util.AppUtil;
import com.highlightmusicgroup.util.Const;
import com.highlightmusicgroup.util.Prefs;
import com.highlightmusicgroup.util.RecyclerViewPositionHelper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.highlightmusicgroup.playerManager.PlayerUtility.runOnUIThread;

public class VideoFragment extends Fragment {

    private static VideoFragment instance = null;
    FragmentVideoBinding binding;

    private int LIMIT = 20, CURRENT_PAGE = 1, LAST_PAGE = 100;
    private AppManageInterface appManageInterface;
    private String path, type, pageTitle, id;
    private Context context;
    private FragmentManager fragmentManager;
    private Tracker mTracker;
    private boolean loadingInProgress, isInner;
    private VideoListingAdapter videoListingAdapter;
    public static boolean isFromVideoFragment = true;

    public static synchronized VideoFragment getInstance() {
        return instance;
    }

    public static synchronized VideoFragment newInstance() {
        return instance = new VideoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTracker = HMG.getDefaultTracker();
        binding = FragmentVideoBinding.inflate(inflater, container, false);
        isFromVideoFragment = true;
        LiveTvFragment.isfromLiveTvFragment = false;
        HomeScreen.isPipModeEnabled = true;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Video Listing");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        binding.listing.setVisibility(View.GONE);
        binding.noDataFound.setVisibility(View.GONE);
        binding.loader.setVisibility(View.GONE);

        if(HomeScreen.radioStatus == 1){
            Glide.with(this)
                    .load(R.raw.radio_gif)
                    .into(binding.radio);
        }else{
            Glide.with(this)
                    .load(R.drawable.selector_home_radio)
                    .into(binding.radio);
        }

//        appManageInterface.setPlayer();
        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
            id = getArguments().getString("id");
            isInner = getArguments().getBoolean("isInner", false);
        }
        if (type != null && path != null) {
            CURRENT_PAGE = 1;
            LAST_PAGE = 1;
            LIMIT = 20;
            binding.listing.setLayoutManager(new GridLayoutManager(context, 2));
            binding.listing.setHasFixedSize(true);
            if (type.toLowerCase().equals("VideosCategory".toLowerCase())) {
                videoListingAdapter = new VideoListingAdapter(context, fragmentManager, new ArrayList<>(), getResources().getString(R.string.type_horizontal));
                binding.ivVideo.setSelected(true);
            } else if (type.toLowerCase().equals("My favorite videos".toLowerCase())) {
                binding.drawerPopup.setSelected(true);
                binding.tvTitle.setText("My Favorite Videos");
                binding.back.setVisibility(View.VISIBLE);
                videoListingAdapter = new VideoListingAdapter(context, fragmentManager, new ArrayList<>(), getResources().getString(R.string.type_fav));
            } else {
                binding.ivVideo.setSelected(true);
                binding.tvTitle.setText(pageTitle);
                binding.back.setVisibility(View.VISIBLE);
                videoListingAdapter = new VideoListingAdapter(context, fragmentManager, new ArrayList<>(), getResources().getString(R.string.type_vertical));
            }
            binding.listing.setAdapter(videoListingAdapter);
            binding.listing.setVisibility(View.VISIBLE);
            RecyclerViewPositionHelper positionHelper = RecyclerViewPositionHelper.createHelper(binding.listing);

            binding.listing.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0) {
                        LinearLayoutManager layoutManager = (LinearLayoutManager) binding.listing.getLayoutManager();
                        if (layoutManager != null) {
                            int total = layoutManager.getItemCount();
                            int currentLastItem = positionHelper.findLastCompletelyVisibleItemPosition() + 1;

                            if (currentLastItem == (total - 10)) {
                                if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                                    loadingInProgress = true;
                                    CURRENT_PAGE++;
                                    if (type.toLowerCase().equals("My favorite videos".toLowerCase())) {
                                        getFavVideosListing();
                                        binding.drawerPopup.setSelected(true);
                                    } else if (type.toLowerCase().equals("Videos".toLowerCase()) && isInner) {
                                        getVideosDetailListing();
                                        binding.ivVideo.setSelected(true);
                                    } else {
                                        getVideosListing();
                                        binding.ivVideo.setSelected(true);
                                    }
                                }
                            }
                        }

                    }
                }
            });

            binding.scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) && scrollY > oldScrollY) {
                        if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                            loadingInProgress = true;
                            CURRENT_PAGE++;
                            if (type.toLowerCase().equals("My favorite videos".toLowerCase())) {
                                binding.drawerPopup.setSelected(true);
                                getFavVideosListing();
                            } else if (type.toLowerCase().equals("Videos".toLowerCase()) && isInner) {
                                getVideosDetailListing();
                                binding.ivVideo.setSelected(true);
                            } else {
                                getVideosListing();
                                binding.ivVideo.setSelected(true);
                            }
                        }
                    }
                }
            });

            if (type.toLowerCase().equals("My favorite videos".toLowerCase())) {
                getFavVideosListing();
                binding.drawerPopup.setSelected(true);
            } else if (type.toLowerCase().equals("Videos".toLowerCase()) && isInner) {
                getVideosDetailListing();
                binding.ivVideo.setSelected(true);
            } else {
                getVideosListing();
                binding.ivVideo.setSelected(true);
            }
        } else {
            binding.noDataFound.setVisibility(View.VISIBLE);
            binding.listing.setVisibility(View.GONE);
            binding.loader.setVisibility(View.GONE);
        }

        binding.back.setOnClickListener(v -> appManageInterface.go_back());

        binding.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(context, HomeScreen.class);
//                startActivity(i);
                appManageInterface.go_back();
            }
        });

        binding.liveTvPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, LiveTvFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManageInterface.setVisibleStatus(false, false, false, false, false);
                AppUtil.setup_Fragment(fragmentManager, RadioFragment.newInstance(), "", getResources().getString(R.string.radio), getResources().getString(R.string.live_radio), "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, VideoFragment.newInstance(), "", "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "FavouritesSongsList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "PlaylistList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.drawerPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, MenuFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void getVideosListing() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody gallery = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_menu_items(gallery, "30", "VideosCategory", LIMIT, CURRENT_PAGE).enqueue(new Callback<MenuItemsResponse>() {
                @Override
                public void onResponse(@NonNull Call<MenuItemsResponse> call, @NonNull Response<MenuItemsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                loadingInProgress = false;
                                videoListingAdapter.add(response.body().getData());
                                LAST_PAGE = response.body().getLastPage();
                                if (CURRENT_PAGE == 1) {
                                    binding.listing.setVisibility(View.VISIBLE);
                                    binding.noDataFound.setVisibility(View.GONE);
                                    binding.loader.setVisibility(View.GONE);
                                } else {
                                    loadingInProgress = false;
                                    if (CURRENT_PAGE == 1) {
                                        binding.loader.setVisibility(View.GONE);
                                        binding.noDataFound.setVisibility(View.VISIBLE);
                                        binding.listing.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                loadingInProgress = false;
                                if (CURRENT_PAGE == 1) {
                                    binding.loader.setVisibility(View.GONE);
                                    binding.noDataFound.setVisibility(View.VISIBLE);
                                    binding.listing.setVisibility(View.GONE);
                                }
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            loadingInProgress = false;
                            if (CURRENT_PAGE == 1) {
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFound.setVisibility(View.VISIBLE);
                                binding.listing.setVisibility(View.GONE);
                                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                                final AlertDialog dialog = new AlertDialog.Builder(context)
                                        .setCancelable(false)
                                        .setView(dialog_view)
                                        .show();

                                if (dialog.getWindow() != null)
                                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                    dialog.dismiss();
                                });
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MenuItemsResponse> call, @NonNull Throwable t) {
                    loadingInProgress = false;
                    if (CURRENT_PAGE == 1) {
                        binding.loader.setVisibility(View.GONE);
                        binding.noDataFound.setVisibility(View.VISIBLE);
                        binding.listing.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }
            });
        } else {
            loadingInProgress = false;
            if (CURRENT_PAGE == 1) {
                binding.loader.setVisibility(View.GONE);
                binding.noDataFound.setVisibility(View.VISIBLE);
                binding.listing.setVisibility(View.GONE);
            }
        }
    }

    private void getVideosDetailListing() {
        Log.d("mytag","getVideosDetailListing method");
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_video_detail = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_category_videos(get_video_detail, "30", id, type, LIMIT, CURRENT_PAGE).enqueue(new Callback<MenuItemsResponse>() {
                @Override
                public void onResponse(@NonNull Call<MenuItemsResponse> call, @NonNull Response<MenuItemsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                loadingInProgress = false;
                                videoListingAdapter.add(response.body().getData());
                                LAST_PAGE = response.body().getLastPage();
                                if (CURRENT_PAGE == 1) {
                                    binding.listing.setVisibility(View.VISIBLE);
                                    binding.noDataFound.setVisibility(View.GONE);
                                    binding.loader.setVisibility(View.GONE);
                                } else {
                                    loadingInProgress = false;
                                    if (CURRENT_PAGE == 1) {
                                        binding.loader.setVisibility(View.GONE);
                                        binding.noDataFound.setVisibility(View.VISIBLE);
                                        binding.listing.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                loadingInProgress = false;
                                if (CURRENT_PAGE == 1) {
                                    binding.loader.setVisibility(View.GONE);
                                    binding.noDataFound.setVisibility(View.VISIBLE);
                                    binding.listing.setVisibility(View.GONE);
                                }
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            loadingInProgress = false;
                            if (CURRENT_PAGE == 1) {
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFound.setVisibility(View.VISIBLE);
                                binding.listing.setVisibility(View.GONE);
                                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                                final AlertDialog dialog = new AlertDialog.Builder(context)
                                        .setCancelable(false)
                                        .setView(dialog_view)
                                        .show();

                                if (dialog.getWindow() != null)
                                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                    dialog.dismiss();
                                });
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MenuItemsResponse> call, @NonNull Throwable t) {
                    loadingInProgress = false;
                    if (CURRENT_PAGE == 1) {
                        binding.loader.setVisibility(View.GONE);
                        binding.noDataFound.setVisibility(View.VISIBLE);
                        binding.listing.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }
            });
        } else {
            loadingInProgress = false;
            if (CURRENT_PAGE == 1) {
                binding.loader.setVisibility(View.GONE);
                binding.noDataFound.setVisibility(View.VISIBLE);
                binding.listing.setVisibility(View.GONE);
            }
        }
    }

    private void getFavVideosListing() {
        Log.d("mytag","getFavVideosListing          method");
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            binding.drawerPopup.setSelected(true);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_fav_videos = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_fav_videos(get_fav_videos, "FavouriteVideosList", LIMIT, CURRENT_PAGE).enqueue(new Callback<MenuItemsResponse>() {
                @Override
                public void onResponse(@NonNull Call<MenuItemsResponse> call, @NonNull Response<MenuItemsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                loadingInProgress = false;
                                videoListingAdapter.add(response.body().getData());
                                Log.d("mytag","api call in favorite-------------"+response.body().getData().size());
                                LAST_PAGE = response.body().getLastPage();
                                if (CURRENT_PAGE == 1) {
                                    binding.listing.setVisibility(View.VISIBLE);
                                    binding.noDataFound.setVisibility(View.GONE);
                                    binding.loader.setVisibility(View.GONE);
                                } else {
                                    loadingInProgress = false;
                                }
                            } else {
                                loadingInProgress = false;
                                if (CURRENT_PAGE == 1) {
                                    binding.loader.setVisibility(View.GONE);
                                    binding.noDataFound.setVisibility(View.VISIBLE);
                                    binding.listing.setVisibility(View.GONE);
                                }
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            loadingInProgress = false;
                            if (CURRENT_PAGE == 1) {
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFound.setVisibility(View.VISIBLE);
                                binding.listing.setVisibility(View.GONE);
                                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                                final AlertDialog dialog = new AlertDialog.Builder(context)
                                        .setCancelable(false)
                                        .setView(dialog_view)
                                        .show();

                                if (dialog.getWindow() != null)
                                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                    dialog.dismiss();
                                });
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MenuItemsResponse> call, @NonNull Throwable t) {
                    loadingInProgress = false;
                    if (CURRENT_PAGE == 1) {
                        binding.loader.setVisibility(View.GONE);
                        binding.noDataFound.setVisibility(View.VISIBLE);
                        binding.listing.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }
            });
        } else {
            loadingInProgress = false;
            if (CURRENT_PAGE == 1) {
                binding.loader.setVisibility(View.GONE);
                binding.noDataFound.setVisibility(View.VISIBLE);
                binding.listing.setVisibility(View.GONE);
            }
        }
    }

    public void setFavVideoList() {
        if (VideoFragment.getInstance() != null)
            runOnUIThread(() -> {
//                favoriteData = new ArrayList<>();
                videoListingAdapter.clear();
                getFavVideosListing();
            });

//            getFavVideosListing();
    }

}
