package com.highlightmusicgroup.playerManager;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.highlightmusicgroup.data.models.asset.SongDetails;
import com.highlightmusicgroup.util.Const;
import com.highlightmusicgroup.util.Prefs;
import com.google.gson.Gson;

import java.util.ArrayList;


public class MusicPreference {

    public static ArrayList<SongDetails> playlist = new ArrayList<>();
    public static ArrayList<SongDetails> shuffledPlaylist = new ArrayList<>();
    public static SongDetails playingSongDetail;

    public static SharedPreferences getPreference(Context context) {
        return context.getSharedPreferences("DjDrake", Activity.MODE_PRIVATE);
    }

    public static void saveLastSong(Context context, SongDetails mDetail) {
        SharedPreferences.Editor editor = getPreference(context).edit();
        Gson gson = new Gson();
        String lastPlaySong = gson.toJson(mDetail);
        editor.putString("lastPlaySong", lastPlaySong);
        editor.apply();
    }

    public static SongDetails getLastSong(Context context) {
        if (playingSongDetail == null) {
            SharedPreferences mSharedPreferences = getPreference(context);
            String lastPlaySong = mSharedPreferences.getString("lastPlaySong", "");
            Gson gson = new Gson();
            playingSongDetail = gson.fromJson(lastPlaySong, SongDetails.class);
        }
        return playingSongDetail;
    }

    public static void saveLastPosition(Context context, int position) {
        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putInt("lastPosition", position);
        editor.apply();
    }

    public static int getLastPosition(Context context) {
        SharedPreferences mSharedPreferences = getPreference(context);
        return mSharedPreferences.getInt("lastPosition", 0);
    }

    public static void saveLastPath(Context context, String path) {
        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putString("path", path);
        editor.apply();
    }

    public static String getLastPath(Context context) {
        SharedPreferences mSharedPreferences = getPreference(context);
        return mSharedPreferences.getString("path", "");
    }

    public static void setRepeat(Context context, int isOn) {
        Prefs.getPrefInstance().setValue(context, Const.REPEAT, isOn);
    }

    public static int getRepeat(Context context) {
        return Prefs.getPrefInstance().getValue(context, Const.REPEAT, 0);
    }


    public static void setShuffle(Context context, boolean isOn) {
        Prefs.getPrefInstance().setValue(context, Const.SHUFFLE, isOn);
    }

    public static boolean getShuffle(Context context) {
        return Prefs.getPrefInstance().getValue(context, Const.SHUFFLE, false);
    }

    public static void setAutoPlay(Context context, boolean isOn) {
        Prefs.getPrefInstance().setValue(context, Const.ISAUTOPLAY, isOn);
    }

    public static boolean getAutoPlay(Context context) {
        return Prefs.getPrefInstance().getValue(context, Const.ISAUTOPLAY, false);
    }

}