/*
 * Copyright @imsaumilshah.
 */
package com.highlightmusicgroup.playerManager;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.RemoteControlClient;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.media.session.MediaSessionCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.bumptech.glide.request.target.NotificationTarget;
import com.highlightmusicgroup.R;
import com.highlightmusicgroup.activities.HomeScreen;
import com.highlightmusicgroup.application.HMG;
import com.highlightmusicgroup.data.models.asset.SongDetails;

public class MusicPlayerService extends Service implements NotificationManager.NotificationCenterDelegate {

    public static final String NOTIFY_PREVIOUS = "HMG.Previous";
    public static final String NOTIFY_CLOSE = "HMG.Close";
    public static final String NOTIFY_PAUSE = "HMG.Pause";
    public static final String NOTIFY_PLAY = "HMG.Play";
    public static final String NOTIFY_NEXT = "HMG.Next";
    public static Notification notification;
    //    public static RemoteViews simpleContentView;
    public static RemoteViews simpleContentView;
    //    public static RemoteViews expandedView = null;
    public static RemoteViews expandedView;
    // public  final String NOTIFICATION_CHANNEL_ID = "PlayerNotification";
    static AsyncTask asyncTask;
    static int flag;
    private static boolean ignoreAudioFocus = false;
    public NotificationTarget notificationTarget;
    MediaSessionCompat mMediaSession;
    RemoteControlClient mRemoteControlClient;
    android.app.NotificationManager notificationManager;
    MediaMetadataRetriever metaRetriver;
    AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener;
    private int isPlaying = 0;
    private NotificationManagerCompat mNotificationManager;
    private RemoteControlClient remoteControlClient;
    private AudioManager audioManager;
    private PhoneStateListener phoneStateListener;

    public static void setIgnoreAudioFocus() {
        ignoreAudioFocus = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioPlayStateChanged);
        try {
            phoneStateListener = new PhoneStateListener() {
                @Override
                public void onCallStateChanged(int state, String incomingNumber) {
                    if (state == TelephonyManager.CALL_STATE_RINGING) {
                        if (MediaController.getInstance().getPlayingSongDetail() != null) {
                            if (!MediaController.getInstance().isAudioPaused()) {
                                flag = 1;
                                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                            }
                        }
                    } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                        if (MediaController.getInstance().getPlayingSongDetail() != null) {
                            if (MediaController.getInstance().isAudioPaused()) {
                                if (flag == 1) {
                                    flag = 0;
                                    MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                                }
                            }
                        }
                    } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                        if (MediaController.getInstance().getPlayingSongDetail() != null) {
                            if (MediaController.getInstance().isAudioPaused()) {
                                if (flag == 1) {
                                    flag = 0;
                                    MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                                }
                            }
                        }
                    }
                    super.onCallStateChanged(state, incomingNumber);
                }
            };
            TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (mgr != null) {
                mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOnAudioFocusChangeListener = focusChange -> {
            if (ignoreAudioFocus) {
                ignoreAudioFocus = false;
                return;
            }
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                // Pause
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    if (!MediaController.getInstance().isAudioPaused()) {
                        isPlaying = 1;
                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                    }
                }
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                MediaController.getInstance().setduckvolume();
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK) {
                ignoreAudioFocus = true;
                MediaController.getInstance().setnormalvolume();
            }else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                audioManager.abandonAudioFocus(mOnAudioFocusChangeListener);

                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    if (!MediaController.getInstance().isAudioPaused()) {
                        isPlaying = 1;
                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                    }
                }
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                ignoreAudioFocus = false;
                if (isPlaying == 1) {
                    isPlaying = 0;
                    MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                }
            }
        };
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            SongDetails messageObject = MediaController.getInstance().getPlayingSongDetail();
            if (messageObject == null) {
                PlayerUtility.runOnUIThread(this::stopSelf);
                return START_STICKY;
            }
            ComponentName remoteComponentName = new ComponentName(getApplicationContext(), MusicPlayerReceiver.class.getName());
            try {
                if (remoteControlClient == null) {
                    audioManager.registerMediaButtonEventReceiver(remoteComponentName);
                    Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
                    mediaButtonIntent.setComponent(remoteComponentName);
                    PendingIntent mediaPendingIntent = PendingIntent.getBroadcast(this, 100, mediaButtonIntent.setPackage(getPackageName()), 0);
                    remoteControlClient = new RemoteControlClient(mediaPendingIntent);
                    audioManager.registerRemoteControlClient(remoteControlClient);
                }
                remoteControlClient.setTransportControlFlags(RemoteControlClient.FLAG_KEY_MEDIA_PLAY | RemoteControlClient.FLAG_KEY_MEDIA_PAUSE
                        | RemoteControlClient.FLAG_KEY_MEDIA_PLAY_PAUSE | RemoteControlClient.FLAG_KEY_MEDIA_STOP
                        | RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS | RemoteControlClient.FLAG_KEY_MEDIA_NEXT);
            } catch (Exception e) {
                e.printStackTrace();
            }

            createNotification(messageObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }

    private void createNotification(SongDetails SongDetails) {
        try {
            String songName = SongDetails.getSongName();
            String songArtist = "";

            if(SongDetails.getSongArtist() != null && !SongDetails.getSongArtist().isEmpty()){
                songArtist = SongDetails.getSongArtist();
            }
            String songImage = SongDetails.getSongImage();

            com.highlightmusicgroup.data.models.asset.SongDetails audioInfo = MediaController.getInstance().getPlayingSongDetail();

            simpleContentView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.player_small_notification);
            expandedView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.player_big_notification);

            Intent intent = new Intent(HMG.applicationContext, HomeScreen.class);
            intent.setAction("openPlayer");
            intent.setPackage(getApplicationContext().getPackageName());
            PendingIntent contentIntent = PendingIntent.getActivity(HMG.applicationContext, 100, intent.setPackage(getPackageName()), 0);

            notificationManager = (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            new NotificationCompat.Builder(getApplicationContext());
            NotificationCompat.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = android.app.NotificationManager.IMPORTANCE_LOW;
                NotificationChannel notificationChannel = new NotificationChannel(getPackageName(), getPackageName(), importance);
                notificationChannel.setDescription(getResources().getString(R.string.app_name));
                notificationChannel.setShowBadge(true);
                notificationManager.createNotificationChannel(notificationChannel);
                builder = new NotificationCompat.Builder(getApplicationContext(), getPackageName());
                notification = builder
                        .setChannelId(getPackageName()).setSmallIcon(R.drawable.app_logo)
                        .setContentIntent(contentIntent).setNumber(0).setContentTitle(songName).build();
            } else {
                builder = new NotificationCompat.Builder(getApplicationContext());
                notification = builder
                        .setChannelId(getPackageName()).setSmallIcon(R.drawable.app_logo)
                        .setContentIntent(contentIntent).setContentTitle(songName).build();
            }

            notification.contentView = simpleContentView;
            setListeners(simpleContentView);
            notification.bigContentView = expandedView;
            setListeners(expandedView);
//            notification.contentView.setViewVisibility(R.id.player_progress_bar, View.GONE);
//                notification.bigContentView.setViewVisibility(R.id.player_progress_bar, View.GONE);

            if(audioInfo.getType() != null && audioInfo.getType().toLowerCase().equals(HMG.applicationContext.getResources().getString(R.string.radio).toLowerCase())){
                notification.contentView.setViewVisibility(R.id.song_next_notification, View.GONE);
                notification.contentView.setViewVisibility(R.id.song_previous_notification, View.GONE);
                notification.bigContentView.setViewVisibility(R.id.song_next_notification, View.GONE);
                notification.bigContentView.setViewVisibility(R.id.song_previous_notification, View.GONE);
            } else {
                notification.contentView.setViewVisibility(R.id.song_next_notification, View.VISIBLE);
                notification.contentView.setViewVisibility(R.id.song_previous_notification, View.VISIBLE);
                notification.bigContentView.setViewVisibility(R.id.song_next_notification, View.VISIBLE);
                notification.bigContentView.setViewVisibility(R.id.song_previous_notification, View.VISIBLE);
            }
            if (MediaController.getInstance().isAudioPaused()) {
//                stopForeground(true);
                notification.contentView.setViewVisibility(R.id.song_pause_notification, View.GONE);
                notification.contentView.setViewVisibility(R.id.song_play_notification, View.VISIBLE);
                notification.bigContentView.setViewVisibility(R.id.song_pause_notification, View.GONE);
                notification.bigContentView.setViewVisibility(R.id.song_play_notification, View.VISIBLE);
            } else {
                notification.contentView.setViewVisibility(R.id.song_pause_notification, View.VISIBLE);
                notification.contentView.setViewVisibility(R.id.song_play_notification, View.GONE);
                notification.bigContentView.setViewVisibility(R.id.song_pause_notification, View.VISIBLE);
                notification.bigContentView.setViewVisibility(R.id.song_play_notification, View.GONE);
            }

            notification.contentView.setTextViewText(R.id.asset_title, songName);
            notification.contentView.setTextViewText(R.id.asset_album, songArtist + " - HMG Music");
//            notification.contentView.setImageViewResource(R.id.asset_image, R.drawable.placeholder);

            notification.bigContentView.setTextViewText(R.id.asset_title, songName);
            notification.bigContentView.setTextViewText(R.id.asset_album,songArtist + " - HMG Music" );
//            notification.bigContentView.setImageViewResource(R.id.asset_image, R.drawable.placeholder);

            notification.flags |= Notification.FLAG_ONGOING_EVENT;
            if (MediaController.getInstance().isAudioPaused()) {
                builder.setOngoing(false);
                notificationManager.notify(5, notification);
//                stopForeground(false);
            } else {
                builder.setOngoing(true);
                startForeground(5, notification);
            }
//            new ImageFromURL().execute(songImage);
            if (remoteControlClient != null) {
                RemoteControlClient.MetadataEditor metadataEditor = remoteControlClient.editMetadata(true);
                metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_ARTIST, songArtist);
                metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_TITLE, songName);
                if (audioInfo != null && audioInfo.getCover(HMG.applicationContext) != null) {
                    metadataEditor.putBitmap(RemoteControlClient.MetadataEditor.BITMAP_KEY_ARTWORK,
                            audioInfo.getCover(HMG.applicationContext));
                }
                metadataEditor.apply();
                if (!MediaController.getInstance().isAudioPaused()) {
                    audioManager.requestAudioFocus(mOnAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setListeners(RemoteViews view) {
        try {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, new Intent(NOTIFY_PREVIOUS).setPackage(getPackageName()), PendingIntent.FLAG_UPDATE_CURRENT);
            view.setOnClickPendingIntent(R.id.song_previous_notification, pendingIntent);
            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, new Intent(NOTIFY_CLOSE).setPackage(getPackageName()), PendingIntent.FLAG_UPDATE_CURRENT);
//            view.setOnClickPendingIntent(R.id.player_close, pendingIntent);
            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, new Intent(NOTIFY_PAUSE).setPackage(getPackageName()), PendingIntent.FLAG_UPDATE_CURRENT);
            view.setOnClickPendingIntent(R.id.song_pause_notification, pendingIntent);
            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, new Intent(NOTIFY_NEXT).setPackage(getPackageName()), PendingIntent.FLAG_UPDATE_CURRENT);
            view.setOnClickPendingIntent(R.id.song_next_notification, pendingIntent);
            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, new Intent(NOTIFY_PLAY).setPackage(getPackageName()), PendingIntent.FLAG_UPDATE_CURRENT);
            view.setOnClickPendingIntent(R.id.song_play_notification, pendingIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        MediaController.getInstance().cleanupPlayer(HMG.applicationContext, true, true);
        super.onTaskRemoved(rootIntent);
        android.app.NotificationManager notificationManager = (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
        int info = ServiceInfo.FLAG_STOP_WITH_TASK;
        stopSelf(info);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (remoteControlClient != null) {
            RemoteControlClient.MetadataEditor metadataEditor = remoteControlClient.editMetadata(true);
            metadataEditor.clear();
            metadataEditor.apply();
            audioManager.unregisterRemoteControlClient(remoteControlClient);
            audioManager.abandonAudioFocus(mOnAudioFocusChangeListener);
            notification.contentIntent.cancel();
        }
        try {
            TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (mgr != null) {
                mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioPlayStateChanged);
    }

    @Override
    public void didReceivedNotification(int id, Object... args) {
        if (id == NotificationManager.audioPlayStateChanged) {
            SongDetails SongDetails = MediaController.getInstance().getPlayingSongDetail();
            if (SongDetails != null) {
                createNotification(SongDetails);
            } else {
                stopSelf();
            }
        }
    }

    @Override
    public void newSongLoaded(Object... args) {
    }
}

