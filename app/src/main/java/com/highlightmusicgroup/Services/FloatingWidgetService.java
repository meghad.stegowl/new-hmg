package com.highlightmusicgroup.Services;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.highlightmusicgroup.R;
import com.highlightmusicgroup.activities.HomeScreen;
import com.highlightmusicgroup.util.AppUtil;
import com.highlightmusicgroup.util.Const;
import com.highlightmusicgroup.util.Prefs;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;

import static com.highlightmusicgroup.fragments.LiveTvFragment.closeLoveTvPlayer;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_OUT_OF_MEMORY;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_REMOTE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_RENDERER;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_SOURCE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_UNEXPECTED;

public class FloatingWidgetService extends Service {

    public FloatingWidgetService() {
    }

    public static WindowManager mWindowManager;
    public static View mFloatingWidget;
    Uri videoUri;
    public static SimpleExoPlayer exoPlayer;
    PlayerView playerView;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            String uriStr = intent.getStringExtra("videoUri");
            String isfrom = intent.getStringExtra("isfrom");
//            String currentPosition = intent.getStringExtra("currentPosition");
            try {
                videoUri = Uri.parse(uriStr);
            }catch (Exception e){

            }

            if (mWindowManager != null && mFloatingWidget.isShown() && exoPlayer != null) {
                mWindowManager.removeView(mFloatingWidget);
                mFloatingWidget = null;
                mWindowManager = null;
                exoPlayer.setPlayWhenReady(false);
                exoPlayer.release();
                exoPlayer = null;
            }
            final WindowManager.LayoutParams params;
            mFloatingWidget = LayoutInflater.from(this).inflate(R.layout.custom_pop_up_window, null);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                params = new WindowManager.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);
            } else {
                params = new WindowManager.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);
            }

            params.gravity = Gravity.TOP | Gravity.LEFT;
            params.x = 200;
            params.y = 200;

            mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
            mWindowManager.addView(mFloatingWidget, params);

//            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
//            TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
//            exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);

            playerView = mFloatingWidget.findViewById(R.id.playerView);
            ImageView imageviewClose = mFloatingWidget.findViewById(R.id.imageViewDismiss);
            ImageView imageviewMaximize = mFloatingWidget.findViewById(R.id.imageViewMaximize);

            imageviewMaximize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mWindowManager != null && mFloatingWidget.isShown() && exoPlayer != null) {
                        mWindowManager.removeView(mFloatingWidget);
                        mFloatingWidget = null;
                        mWindowManager = null;
                        exoPlayer.setPlayWhenReady(false);
                        exoPlayer.release();
                        exoPlayer = null;
                        stopSelf();

                        if (isfrom.equals("livetv")) {
                            Intent intent1 = new Intent(FloatingWidgetService.this, HomeScreen.class);
                            intent1.putExtra("videoUri", videoUri.toString());
                            intent1.putExtra("isfrom", "livetv");
                            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent1);
                        } else {
                            Intent intent1 = new Intent(FloatingWidgetService.this, HomeScreen.class);
                            intent1.putExtra("videoUri", videoUri.toString());
                            intent1.putExtra("isfromvideo", "isfromvideo");
                            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent1);
                        }

                    }
                }
            });

            imageviewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mWindowManager != null && mFloatingWidget.isShown() && exoPlayer != null) {
                        mWindowManager.removeView(mFloatingWidget);
                        mFloatingWidget = null;
                        mWindowManager = null;
                        exoPlayer.setPlayWhenReady(false);
                        exoPlayer.release();
                        exoPlayer = null;
                        stopSelf();
                    }
                }
            });

            if (isfrom != null) {
                if (isfrom.equals("livetv")) {
                    initializePlayer();
                }else{
                    playVideo();
                }
            }

            mFloatingWidget.findViewById(R.id.relativeLayoutPopUp).setOnTouchListener(new View.OnTouchListener() {
                private int initialX, initialY;
                private float initialTouchX, initialTouchY;

                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            initialX = params.x;
                            initialY = params.y;
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();
                            return true;

                        case MotionEvent.ACTION_UP:
                            return true;

                        case MotionEvent.ACTION_MOVE:
                            params.x = initialX + (int) (event.getRawX() - initialTouchX);
                            params.y = initialY + (int) (event.getRawY() - initialTouchY);
                            mWindowManager.updateViewLayout(mFloatingWidget, params);
                            return true;
                    }
                    return false;
                }
            });

        }

        return super.onStartCommand(intent, flags, startId);
    }

    public static void hidePipPlayer(){
        if (mWindowManager != null && mFloatingWidget.isShown() && FloatingWidgetService.exoPlayer != null) {
            mWindowManager.removeView(mFloatingWidget);
            mFloatingWidget = null;
            mWindowManager = null;
            FloatingWidgetService.exoPlayer.setPlayWhenReady(false);
            FloatingWidgetService. exoPlayer.release();
            FloatingWidgetService. exoPlayer = null;
        }
    }


    public void playVideo(/*String currentPosition*/) {
//        closeVideoTVPlayer();
        try {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
            exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);

            String playerInfo = Util.getUserAgent(this, "VideoPlayer");
            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this, playerInfo);
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            MediaSource mediaSource = new ExtractorMediaSource(videoUri, dataSourceFactory, extractorsFactory, null, null);
            playerView.setPlayer(exoPlayer);
            exoPlayer.prepare(mediaSource, true , true);
            exoPlayer.setPlayWhenReady(true);
            long currentPosition = HomeScreen.getVideoPosition();
            exoPlayer.seekTo(currentPosition);
            Prefs.getPrefInstance().setValue(this, Const.VIDEO_SEEKBAR_POSITION, String.valueOf(currentPosition));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initializePlayer() {
        closeLoveTvPlayer();
        String playBackUrl = String.valueOf(videoUri);
        exoPlayer = new SimpleExoPlayer.Builder(this).build();
        playerView.setPlayer(exoPlayer);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        playerView.setUseController(true);

        exoPlayer.setPlayWhenReady(true);
        exoPlayer.seekTo(0);
        Prefs.getPrefInstance().setValue(this, Const.VIDEO_URL, playBackUrl);

        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.CONTENT_TYPE_MOVIE)
                .build();
        if (exoPlayer.getAudioComponent() != null)
            exoPlayer.getAudioComponent().setAudioAttributes(audioAttributes, true);
        try {
            if (!playBackUrl.equals("")) {
                DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, getResources().getString(R.string.app_name)));
                MediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(playBackUrl));
                exoPlayer.prepare(videoSource, true, true);
            }
        } catch (Exception e) {

        }
        exoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                switch (error.type) {
                    case TYPE_SOURCE:
                        break;
                    case TYPE_RENDERER:
                    case TYPE_UNEXPECTED:
                        exoPlayer.retry();
                        break;
                    case TYPE_OUT_OF_MEMORY:
                    case TYPE_REMOTE:
                        AppUtil.show_Snackbar(getApplicationContext(), playerView, error.getMessage(), true);
                        break;
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mFloatingWidget != null)
            mWindowManager.removeView(mFloatingWidget);

    }
}
