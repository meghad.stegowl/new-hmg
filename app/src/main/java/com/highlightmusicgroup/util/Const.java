package com.highlightmusicgroup.util;

public class Const {

    /**
     * Preference file name where you can store data in device
     */
    public static final String PREFS_FILENAME = "DjDrake";
    public static final String SELECTED_LANGUAGE = "selected_language";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_NAME_EMAIL_CHECKED = "user_name_checked";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String KEEP_USER_LOGGED_IN = "keepUserLoggedIn";
    public static final String USER_FULL_NAME = "user_full_name";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String TIME = "time";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String LOGIN_ACCESS = "login";
    public static final String FCM_ID = "fcm_id";
    public static final String TOKEN  = "token";
    public static final String SHUFFLE = "shuffle";
    public static final String ISAUTOPLAY = "isautoplay";
    public static final String REPEAT = "repeat";
    public static final String MENU_LIVE_TV_STATUS = "menu_live_tv_status";
    public static final String MENU_RADIO_STATUS = "menu_radio_status";
    public static final String MENU_LIVE_TV_ID = "menu_live_tv_id";
    public static final String MENU_RADIO_ID = "menu_radio_id";
    public static final String MENU_VIDEO = "menu_video";
    public static final String INSTAGRAM_PATH = "instagram_path";
    public static boolean isSwipe = false;
    public static boolean isPopupClose = false;
//    public static boolean isRadioPlayed = false;
    public static final String NOW_PLAYING = "nowplaying";
    public static final String APP_STATUS = "appstatus";
    public static final String VIDEO_POSITION = "video_position";
    public static final String VIDEO_TYPE = "video_type";
    public static final String VIDEO_URL_TYPE = "video_url_type";
    public static final String VIDEO_URL = "video_url";
    public static final String VIDEO_IMAGE = "video_image";
    public static final String Google_add_key = "googleadskey";
    public static final String INTERESTITIAL_KEY = "android_interstitial";
    public static final String VIDEO_YOUTUBE_URL = "video_youtube_url";
    public static final String VIDEO_SEEKBAR_POSITION = "video_seekbar";
    public static final String SET_PROFILE_IMAGE = "profile_image";
//    public static final String BANNER_ADS = "banner_ads";
//    public static final String INTERSTITIAL_ADS = "Interstitial_ads";

//    Total API

//    private static final String HOST = "http://54.146.63.52/djkennyadmin/api/";
//    private static final String HOST = "http://54.236.68.2/HMGNewWeb/api/";
    private static final String HOST = "https://highlightmusicgroup.com/HMGNewWeb/api/";
    private static final String APPEND = "";
    private static final String GEO_APPEND = "";
    public static final String HMG_HOST = HOST;

    public static final String LOGIN_USER = HMG_HOST + "signin";
    public static final String WEB_VIEW = HMG_HOST + "legaldetails";
    public static final String REGISTER_USER = HMG_HOST + "signup";
    public static final String PROFILE = HMG_HOST + "profile";
    public static final String LOGOUT = HMG_HOST + "signout";
    public static final String SKIP_USER = HMG_HOST + "skipuser";
    public static final String FORGOT_PASSWORD = HMG_HOST + "forgotpassword";
    public static final String CHANGE_PASSWORD = HMG_HOST + "changepassword";
    public static final String UPLOAD_PICTURE = HMG_HOST + "uploads";
    public static final String LIVE_TV_SLIDER = HMG_HOST + "sponsorslider";
    public static final String GALLERY = HMG_HOST + "gallery";
    public static final String HOME_CAROUSAL = HMG_HOST + "homeslider";
    public static final String HOME_API = HMG_HOST + "home";
    public static final String HOME_GRID_CATEGORY_API = HMG_HOST + "audiosubcategories/{category_id}";
    public static final String HOME_ITEMS = HMG_HOST + "homeitems";
    public static final String SHARE_MY_APP = HMG_HOST + "sharemyapp";
    public static final String GET_NOTIFICATIONS = HMG_HOST + "notifications";
    public static final String CALL_SONG_PLAY = HMG_HOST + "playcount";
    public static final String SOCIAL_MEDIA = HMG_HOST + "socialmedia";
    public static final String BIOGRAPHY = HMG_HOST + "biography";
    public static final String MENU_ITEMS = HMG_HOST + "menuitems";
    public static final String GET_SEARCH = HMG_HOST + "search";
    public static final String CATEGORY_ITEMS = HMG_HOST + "categoryitems";
    public static final String MUSIC_SONGS = HMG_HOST + "menuitems";
    public static final String CATEGORY_ITEM_DETAILS = HMG_HOST + "categoryitemdetails";
    public static final String TOTAL_SHARED = HMG_HOST + "totalshared";
    public static final String FAV_VIDEOS = HMG_HOST + "favouritevideos";
    public static final String SONG_LIKE = HMG_HOST + "likes";
    public static final String FAVORITE_SONG = HMG_HOST + "favourites";
    public static final String PLAYLIST = HMG_HOST + "playlist";
    public static final String ADD_PLAYLIST_SONGS = HMG_HOST + "playlistsongs";
    public static final String GET_FAVORITES = HMG_HOST + "favourites";
    public static final String SUBSCIPTIONS = HMG_HOST + "subscriptions";
    public static final String BOOKINGS = HMG_HOST + "booking";
    public static final String GET_INSTAGRAM = HMG_HOST + "instagramlink";
    public static final String SUPPORT = HMG_HOST + "support";
    public static final String DRAWER_ITEMS = HMG_HOST + "menu";
    public static final String VIDEO_LIKE_UNLIKE = HMG_HOST + "videolikedislike";
    public static final String GET_HEADER_MENU_STATUS = HMG_HOST + "menustatus";
    public static final String BANNER_SLIDER = HMG_HOST + "bannerslider";
    public static final String Intro_SLIDER = HMG_HOST + "walkthrough\n";
    public static final String GOOGLE_AD = HMG_HOST + "googleads";


}