package com.highlightmusicgroup.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeCategoryData {
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("parent_category_name")
    @Expose
    private String parentCategoryName;
    @SerializedName("menu_id")
    @Expose
    private Integer menuId;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_for")
    @Expose
    private String categoryFor;
    @SerializedName("image_status")
    @Expose
    private Boolean imageStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("song_id")
    @Expose
    private Integer songId;
    @SerializedName("song_name")
    @Expose
    private String songName;
    @SerializedName("song_artist")
    @Expose
    private String songArtist;
    @SerializedName("song_image")
    @Expose
    private String songImage;
    @SerializedName("song")
    @Expose
    private String song;
    @SerializedName("song_duration")
    @Expose
    private String songDuration;
    @SerializedName("likes_count")
    @Expose
    private Integer likesCount;
    @SerializedName("likes_status")
    @Expose
    private Boolean likesStatus;
    @SerializedName("favourites_count")
    @Expose
    private Integer favouritesCount;
    @SerializedName("favourites_status")
    @Expose
    private Boolean favouritesStatus;
    @SerializedName("total_played")
    @Expose
    private Integer totalPlayed;
    @SerializedName("total_shared")
    @Expose
    private Integer totalShared;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryFor() {
        return categoryFor;
    }

    public void setCategoryFor(String categoryFor) {
        this.categoryFor = categoryFor;
    }

    public Boolean getImageStatus() {
        return imageStatus;
    }

    public void setImageStatus(Boolean imageStatus) {
        this.imageStatus = imageStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getSongId() {
        return songId;
    }

    public void setSongId(Integer songId) {
        this.songId = songId;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSongArtist() {
        return songArtist;
    }

    public void setSongArtist(String songArtist) {
        this.songArtist = songArtist;
    }

    public String getSongImage() {
        return songImage;
    }

    public void setSongImage(String songImage) {
        this.songImage = songImage;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public String getSongDuration() {
        return songDuration;
    }

    public void setSongDuration(String songDuration) {
        this.songDuration = songDuration;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public Boolean getLikesStatus() {
        return likesStatus;
    }

    public void setLikesStatus(Boolean likesStatus) {
        this.likesStatus = likesStatus;
    }

    public Integer getFavouritesCount() {
        return favouritesCount;
    }

    public void setFavouritesCount(Integer favouritesCount) {
        this.favouritesCount = favouritesCount;
    }

    public Boolean getFavouritesStatus() {
        return favouritesStatus;
    }

    public void setFavouritesStatus(Boolean favouritesStatus) {
        this.favouritesStatus = favouritesStatus;
    }

    public Integer getTotalPlayed() {
        return totalPlayed;
    }

    public void setTotalPlayed(Integer totalPlayed) {
        this.totalPlayed = totalPlayed;
    }

    public Integer getTotalShared() {
        return totalShared;
    }

    public void setTotalShared(Integer totalShared) {
        this.totalShared = totalShared;
    }
}
