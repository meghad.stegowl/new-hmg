package com.highlightmusicgroup.data.models.music;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddPlaylistResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("playlist_id")
    @Expose
    private String playlistId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("playlist_name")
    @Expose
    private String playlistName;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String playlistName) {
        this.playlistName = playlistName;
    }
}
