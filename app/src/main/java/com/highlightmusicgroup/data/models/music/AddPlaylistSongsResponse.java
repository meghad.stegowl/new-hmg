package com.highlightmusicgroup.data.models.music;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddPlaylistSongsResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("playlistsongs_id")
    @Expose
    private String playlistsongsId;
    @SerializedName("playlist_id")
    @Expose
    private String playlistId;
    @SerializedName("song_id")
    @Expose
    private String songId;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPlaylistsongsId() {
        return playlistsongsId;
    }

    public void setPlaylistsongsId(String playlistsongsId) {
        this.playlistsongsId = playlistsongsId;
    }

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }
}
