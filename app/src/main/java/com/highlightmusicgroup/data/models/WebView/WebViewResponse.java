package com.highlightmusicgroup.data.models.WebView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WebViewResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("terms_data")
    @Expose
    private List<WebViewDataTerms> termsData = null;
    @SerializedName("privacy_data")
    @Expose
    private List<WebViewDataPrivacy> privacyData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WebViewDataTerms> getTermsData() {
        return termsData;
    }

    public void setTermsData(List<WebViewDataTerms> termsData) {
        this.termsData = termsData;
    }

    public List<WebViewDataPrivacy> getPrivacyData() {
        return privacyData;
    }

    public void setPrivacyData(List<WebViewDataPrivacy> privacyData) {
        this.privacyData = privacyData;
    }


}
