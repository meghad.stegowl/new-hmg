package com.highlightmusicgroup.data.models.video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FavVideosResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("favourites_status")
    @Expose
    private Boolean favouritesStatus;
    @SerializedName("favourites_count")
    @Expose
    private Integer favouritesCount;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getFavouritesStatus() {
        return favouritesStatus;
    }

    public void setFavouritesStatus(Boolean favouritesStatus) {
        this.favouritesStatus = favouritesStatus;
    }

    public Integer getFavouritesCount() {
        return favouritesCount;
    }

    public void setFavouritesCount(Integer favouritesCount) {
        this.favouritesCount = favouritesCount;
    }
}
