package com.highlightmusicgroup.data.models.music;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TotalSharedResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("total_shared_status")
    @Expose
    private Boolean totalSharedStatus;
    @SerializedName("total_shared")
    @Expose
    private String totalShared;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getTotalSharedStatus() {
        return totalSharedStatus;
    }

    public void setTotalSharedStatus(Boolean totalSharedStatus) {
        this.totalSharedStatus = totalSharedStatus;
    }

    public String getTotalShared() {
        return totalShared;
    }

    public void setTotalShared(String totalShared) {
        this.totalShared = totalShared;
    }
}
