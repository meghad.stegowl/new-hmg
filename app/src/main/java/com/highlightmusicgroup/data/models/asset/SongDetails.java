package com.highlightmusicgroup.data.models.asset;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SongDetails {

    public float audioBufferProgress = 0.0f;
    public float audioProgress = 0.0f;
    public int audioProgressSec = 0;
    public int audioProgressTotalSec = 0;
    private String type;
    private String contentType;
    @SerializedName("song_id")
    @Expose
    private String songId;
    @SerializedName("song_name")
    @Expose
    private String songName;
    @SerializedName("song_artist")
    @Expose
    private String songArtist;
    @SerializedName("song_image")
    @Expose
    private String songImage;
    @SerializedName("song")
    @Expose
    private String song;
    @SerializedName("song_duration")
    @Expose
    private String songDuration;
    @SerializedName("likes_count")
    @Expose
    private String likesCount;
    @SerializedName("likes_status")
    @Expose
    private Boolean likesStatus;
    @SerializedName("favourites_count")
    @Expose
    private String favouritesCount;
    @SerializedName("favourites_status")
    @Expose
    private Boolean favouritesStatus;
    @SerializedName("total_played")
    @Expose
    private String totalPlayed;
    @SerializedName("total_shared")
    @Expose
    private String totalShared;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("favourites_id")
    @Expose
    private String favouritesId;
    @SerializedName("playlist_id")
    @Expose
    private String playlistId;
    @SerializedName("playlistsongs_id")
    @Expose
    private String playlistsongsId;
    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("categoryitems_id")
    @Expose
    private String categoryitemsId;
    @SerializedName("total_shared_status")
    @Expose
    private Boolean totalSharedStatus;
    @SerializedName("playlist_status")
    @Expose
    private Boolean playlistStatus;
    @SerializedName("playlist_status_count")
    @Expose
    private String playlistStatusCount;
    @SerializedName("radio_id")
    @Expose
    private String radioId;
    @SerializedName("radio_name")
    @Expose
    private String radioName;
    @SerializedName("radio_image")
    @Expose
    private String radioImage;
    @SerializedName("radio_link")
    @Expose
    private String radioLink;

    public SongDetails(String songId,String menuId, String songImage, String songName, String songArtist, String song, String songDuration) {
        this.songId = songId;
        this.menuId = menuId;
        this.songImage = songImage;
        this.songName = songName;
        this.songArtist = songArtist;
        this.song = song;
        this.songDuration = songDuration;
    }

    public SongDetails() {
    }

    public String getRadioId() {
        return radioId;
    }

    public void setRadioId(String radioId) {
        this.radioId = radioId;
    }

    public String getRadioName() {
        return radioName;
    }

    public void setRadioName(String radioName) {
        this.radioName = radioName;
    }

    public String getRadioImage() {
        return radioImage;
    }

    public void setRadioImage(String radioImage) {
        this.radioImage = radioImage;
    }

    public String getRadioLink() {
        return radioLink;
    }

    public void setRadioLink(String radioLink) {
        this.radioLink = radioLink;
    }

    public String getFavouritesId() {
        return favouritesId;
    }

    public void setFavouritesId(String favouritesId) {
        this.favouritesId = favouritesId;
    }

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public String getPlaylistsongsId() {
        return playlistsongsId;
    }

    public void setPlaylistsongsId(String playlistsongsId) {
        this.playlistsongsId = playlistsongsId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryitemsId() {
        return categoryitemsId;
    }

    public void setCategoryitemsId(String categoryitemsId) {
        this.categoryitemsId = categoryitemsId;
    }

    public Boolean getTotalSharedStatus() {
        return totalSharedStatus;
    }

    public void setTotalSharedStatus(Boolean totalSharedStatus) {
        this.totalSharedStatus = totalSharedStatus;
    }

    public Boolean getPlaylistStatus() {
        return playlistStatus;
    }

    public void setPlaylistStatus(Boolean playlistStatus) {
        this.playlistStatus = playlistStatus;
    }

    public String getPlaylistStatusCount() {
        return playlistStatusCount;
    }

    public void setPlaylistStatusCount(String playlistStatusCount) {
        this.playlistStatusCount = playlistStatusCount;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSongArtist() {
        return songArtist;
    }

    public void setSongArtist(String songArtist) {
        this.songArtist = songArtist;
    }

    public String getSongImage() {
        return songImage;
    }

    public void setSongImage(String songImage) {
        this.songImage = songImage;
    }
    public Bitmap getCover(Context applicationContext) {
        return null;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public String getSongDuration() {
        return songDuration;
    }

    public void setSongDuration(String songDuration) {
        this.songDuration = songDuration;
    }

    public String getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(String likesCount) {
        this.likesCount = likesCount;
    }

    public Boolean getLikesStatus() {
        return likesStatus;
    }

    public void setLikesStatus(Boolean likesStatus) {
        this.likesStatus = likesStatus;
    }

    public String getFavouritesCount() {
        return favouritesCount;
    }

    public void setFavouritesCount(String favouritesCount) {
        this.favouritesCount = favouritesCount;
    }

    public Boolean getFavouritesStatus() {
        return favouritesStatus;
    }

    public void setFavouritesStatus(Boolean favouritesStatus) {
        this.favouritesStatus = favouritesStatus;
    }

    public String getTotalPlayed() {
        return totalPlayed;
    }

    public void setTotalPlayed(String totalPlayed) {
        this.totalPlayed = totalPlayed;
    }

    public String getTotalShared() {
        return totalShared;
    }

    public void setTotalShared(String totalShared) {
        this.totalShared = totalShared;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
