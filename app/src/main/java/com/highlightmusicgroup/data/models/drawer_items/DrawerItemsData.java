package com.highlightmusicgroup.data.models.drawer_items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DrawerItemsData {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("data")
    @Expose
    private ArrayList<DrawerItemsDetails> data = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<DrawerItemsDetails> getData() {
        return data;
    }

    public void setData(ArrayList<DrawerItemsDetails> data) {
        this.data = data;
    }
}
