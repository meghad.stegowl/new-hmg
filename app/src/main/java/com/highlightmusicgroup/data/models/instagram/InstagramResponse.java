package com.highlightmusicgroup.data.models.instagram;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstagramResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("instagramlink_id")
    @Expose
    private String instagramlinkId;
    @SerializedName("link")
    @Expose
    private String link;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getInstagramlinkId() {
        return instagramlinkId;
    }

    public void setInstagramlinkId(String instagramlinkId) {
        this.instagramlinkId = instagramlinkId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
