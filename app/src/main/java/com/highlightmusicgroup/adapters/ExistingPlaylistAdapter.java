package com.highlightmusicgroup.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.highlightmusicgroup.R;
import com.highlightmusicgroup.data.models.asset.SongDetails;
import com.highlightmusicgroup.data.models.music.AddPlaylistResponse;
import com.highlightmusicgroup.data.models.music.ExistingPlaylistData;
import com.highlightmusicgroup.data.remote.APIUtils;
import com.highlightmusicgroup.fragments.PlaylistFavListingFragment;
import com.highlightmusicgroup.util.AppManageInterface;
import com.highlightmusicgroup.util.AppUtil;
import com.highlightmusicgroup.util.Const;
import com.highlightmusicgroup.util.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExistingPlaylistAdapter extends RecyclerView.Adapter<ExistingPlaylistAdapter.ViewHolder> {

    private Context context;
    private List<ExistingPlaylistData> data;
    private boolean canDelete;
    private SongDetails detailsData;
    private AppManageInterface appManageInterface;
    private AlertDialog dialog;
    private FragmentManager fragmentManager;

    public ExistingPlaylistAdapter(Context context, FragmentManager fragmentManager, List<ExistingPlaylistData> data, boolean canDelete, SongDetails detailsData, AlertDialog dialog) {
        this.context = context;
        this.data = data;
        this.fragmentManager = fragmentManager;
        this.canDelete = canDelete;
        this.detailsData = detailsData;
        this.dialog = dialog;
        appManageInterface = (AppManageInterface) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (canDelete)
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_existing_playlist_fragment, parent, false));
        else
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_existing_playlist, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (canDelete) {
            holder.playlistTitle.setText(data.get(position).getPlaylistName());
            holder.container.setOnClickListener(view -> {
                    PlaylistFavListingFragment fragment = PlaylistFavListingFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putString("path", data.get(position).getPlaylistId());
                    bundle.putString("type", context.getResources().getString(R.string.type_playlist_songs));
                    bundle.putString("title", data.get(position).getPlaylistName());
                    fragment.setArguments(bundle);
                    AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
            });
            holder.deletePlaylist.setOnClickListener(view -> {
                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                final AlertDialog dialog = new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setView(dialog_view)
                        .show();

                if (dialog.getWindow() != null)
                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.delete_playlist));
                ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(context.getResources().getString(R.string.yes));
                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.no));
                dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view1 -> {
                    dialog.dismiss();
                    RemovePlaylist(position, view);
                });
                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view1 -> {
                    dialog.dismiss();
                });
            });
        } else {
            holder.playlistName.setText(data.get(position).getPlaylistName());
            holder.view1.setVisibility(position == data.size() - 1 ? View.GONE : View.VISIBLE);
            holder.view2.setVisibility(position == data.size() - 1 ? View.GONE : View.VISIBLE);
            holder.playlistName.setOnClickListener(view -> {
                dialog.dismiss();
                appManageInterface.addSongToPlaylist(detailsData, data.get(position).getPlaylistId());
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private void RemovePlaylist(int position, View view) {
        if (AppUtil.isInternetAvailable(context)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("playlist_id", data.get(position).getPlaylistId());
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody remove_playlist = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().create_playlist(remove_playlist, "PlaylistRemove").enqueue(new Callback<AddPlaylistResponse>() {
                @Override
                public void onResponse(@NonNull Call<AddPlaylistResponse> call, @NonNull Response<AddPlaylistResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            AppUtil.show_Snackbar(context, view , response.body().getMessage(), false);
                                data.remove(position);
                                notifyDataSetChanged();
                            if (data.size() != 0) {
                                notifyDataSetChanged();
                            } else {
                                PlaylistFavListingFragment.getInstance().setNoDataFound();
                            }
                        } else {
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPlaylistResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view1 -> {
                dialog.dismiss();
            });
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Button playlistName;
        TextView playlistTitle;
        View view1;
        View view2;
        LinearLayout container;
        ImageView deletePlaylist;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            if (canDelete) {
                playlistTitle = itemView.findViewById(R.id.playlist_title);

            } else {
                playlistName = itemView.findViewById(R.id.playlist_name);
            }
            view1 = itemView.findViewById(R.id.view1);
            view2 = itemView.findViewById(R.id.view2);
            container = itemView.findViewById(R.id.container);
            deletePlaylist = itemView.findViewById(R.id.delete_playlist);
        }
    }
}
