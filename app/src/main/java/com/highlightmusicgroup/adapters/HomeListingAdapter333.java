package com.highlightmusicgroup.adapters;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.highlightmusicgroup.R;
import com.highlightmusicgroup.application.HMG;
import com.highlightmusicgroup.data.models.BannerSliderData;
import com.highlightmusicgroup.data.models.HomeCategoryData;
import com.highlightmusicgroup.data.models.HomeData;
import com.highlightmusicgroup.fragments.CategoryViewAllFragment;
import com.highlightmusicgroup.util.AppManageInterface;
import com.highlightmusicgroup.util.AppUtil;
import com.highlightmusicgroup.util.ClickableViewPager;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class HomeListingAdapter333 extends RecyclerView.Adapter<HomeListingAdapter333.ViewHolder> {

    public static final int TYPE_CATEGORY = 0, TYPE_MUSIC = 1, TYPE_SLIDER = 2;
    private static Context context;
    private ArrayList<HomeData> homeData = new ArrayList<>();
    private ArrayList<BannerSliderData> bannerSliderData = new ArrayList<>();

    private FragmentManager fragmentManager;
    private AppManageInterface appManageInterface;
    AdapterMainviewPager adapterMainviewPager;
    private final Object progressTimerSync = new Object();
    private final Object sync = new Object();
    private Timer progressTimer = null;

    public HomeListingAdapter333(Context context, FragmentManager fragmentManager, ArrayList<HomeData> homeData
            , ArrayList<BannerSliderData> bannerSliderData) {
        this.context = context;
        this.homeData = homeData;
        this.bannerSliderData = bannerSliderData;
        this.fragmentManager = fragmentManager;
        appManageInterface = (AppManageInterface) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_CATEGORY)
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_home_category, parent, false), viewType);
        else if (viewType == TYPE_MUSIC)
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_main_music, parent, false), viewType);
        else
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_home_slider, parent, false), viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (getItemViewType(position) == TYPE_SLIDER) {
                        adapterMainviewPager = new AdapterMainviewPager(context, bannerSliderData);
                        holder.mPager.setAdapter(adapterMainviewPager);
                        holder.mPager.setPagingEnabled(true);
                        holder.indicator.setViewPager(holder.mPager);
                        startImageSliderTimer(holder);
                    }

                    else if (getItemViewType(position) == TYPE_CATEGORY) {
                        ArrayList<HomeCategoryData> homeCategoryData;
                        homeCategoryData = homeData.get(position).getAssests();
                        int id = homeData.get(position).getCategoryId();
                        holder.title1.setText(homeData.get(position).getName());
                        if (homeCategoryData.size() == 0) {
                            holder.container1.setVisibility(View.GONE);
                            holder.rl_header1.setVisibility(View.GONE);
                            holder.recycler1.setVisibility(View.GONE);
                        } else {
                            holder.recycler1.setHasFixedSize(true);
                            holder.recycler1.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
                            HomeCategoryAdapter adapter = new HomeCategoryAdapter(context, fragmentManager, homeCategoryData);
                            holder.recycler1.setAdapter(adapter);

                            holder.view_all_category.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String menu_id = null;
                                    try {
                                        for (int i = 0; i < homeData.size(); i++) {
                                            menu_id = String.valueOf(homeCategoryData.get(i).getMenuId());
                                        }

                                    } catch (Exception e) {
                                    }
                                    appManageInterface.showFragment();
                                    CategoryViewAllFragment fragment = CategoryViewAllFragment.newInstance();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("path", menu_id);
                                    bundle.putString("type", "GridTracksCategory");
                                    bundle.putString("title", homeData.get(position).getName());
                                    bundle.putString("id", String.valueOf(id));
                                    fragment.setArguments(bundle);
                                    AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
                                }
                            });
                        }
                    }

                    else if (getItemViewType(position) == TYPE_MUSIC) {
                        ArrayList<HomeCategoryData> homeMusicData;
                        holder.title.setText(homeData.get(position).getName());
                        homeMusicData = homeData.get(position).getAssests();
                        String music_name = homeData.get(position).getName();
                        int id = homeData.get(position).getCategoryId();

                        if (homeMusicData.size() == 0) {
                            holder.container.setVisibility(View.GONE);
                            holder.rl_header.setVisibility(View.GONE);
                            holder.recycler.setVisibility(View.GONE);
                        } else {
                            holder.recycler.setHasFixedSize(true);
                            holder.recycler.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
                            HomeMusicAdapter adapter = new HomeMusicAdapter(context, fragmentManager, homeMusicData);
                            holder.recycler.setAdapter(adapter);
                            holder.tv_view_all_music.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String menu_id = null;
                                    try {
                                        for (int i = 0; i < homeData.size(); i++) {
                                            menu_id = String.valueOf(homeMusicData.get(i).getMenuId());
                                        }
                                    } catch (Exception e) {
                                    }
                                    appManageInterface.showFragment();
                                    CategoryViewAllFragment fragment = CategoryViewAllFragment.newInstance();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("path", menu_id);
                                    bundle.putString("type", "Tracks");
                                    bundle.putString("title", music_name);
                                    bundle.putString("id", String.valueOf(id));
                                    fragment.setArguments(bundle);
                                    AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
                                }
                            });
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, 200);
    }


    @Override
    public int getItemCount() {
        if (homeData.size() > 0) {
            return homeData.size();
        } else {
            return homeData.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 1) {
            return TYPE_SLIDER;
        } else {
            if (homeData != null && homeData.get(position).getType() != null) {
                if (homeData.get(position).getType().toLowerCase().equals("Music".toLowerCase())) {
                    return TYPE_MUSIC;
                } else if (homeData.get(position).getType().toLowerCase().equals("TracksCategory".toLowerCase()))
                    return TYPE_CATEGORY;
            }
        }

        return getItemViewType(position);

//        Log.d("mytag", "here is1getItemViewTypegetItemViewType-------------------" + position);
//        int size_cate = homeData.size() - 1;
//        if (position == 1) {
//            Log.d("mytag", "here is11111111111---------------------------------------------" + position);
//            return TYPE_SLIDER;
//        } else if (homeData.get(position).getType().toLowerCase().equals("Music".toLowerCase())) {
//            return TYPE_MUSIC;
//        } else if (homeData.get(position).getType().toLowerCase().equals("TracksCategory".toLowerCase()))
//            return TYPE_CATEGORY;
//        return getItemViewType(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, title1, view_all_category, tv_view_all_music;
        RecyclerView recycler, recycler1;
        RelativeLayout rl_header, rl_header1;
        LinearLayout container, container1;
        ClickableViewPager mPager;
        CircleIndicator indicator;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            title1 = itemView.findViewById(R.id.title1);
            view_all_category = itemView.findViewById(R.id.view_all_category);
            recycler1 = itemView.findViewById(R.id.recycler1);
            recycler = itemView.findViewById(R.id.recycler);
            rl_header = itemView.findViewById(R.id.rl_header);
            rl_header1 = itemView.findViewById(R.id.rl_header1);
            title = itemView.findViewById(R.id.title);
            tv_view_all_music = itemView.findViewById(R.id.tv_view_all_music);
            container = itemView.findViewById(R.id.container);
            container1 = itemView.findViewById(R.id.container1);
            mPager = itemView.findViewById(R.id.mPager);
            indicator = itemView.findViewById(R.id.indicator);
        }
    }

    private void startImageSliderTimer(ViewHolder holder) {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {

                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        runOnUIThread(new Runnable() {
                            @Override
                            public void run() {

                                int currentItem = holder.mPager.getCurrentItem() + 1;
                                int totalItem = adapterMainviewPager.getCount();
                                if (currentItem == totalItem) {
                                    holder.mPager.setCurrentItem(0);
                                } else {
                                    holder.mPager.setCurrentItem(currentItem);
                                }
                            }
                        });
                    }
                }
            }, 0, 5000);
        }
    }

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            HMG.applicationHandler.post(runnable);
        } else {
            HMG.applicationHandler.postDelayed(runnable, delay);
        }
    }
}
